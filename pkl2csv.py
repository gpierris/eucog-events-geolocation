import sys
import pickle

class Participant:
    
    def __init__(self, ID, name, geoLocation, location, website, imageUrl='', event=False):
    
        self.ID = ID
        self.name = name
        self.location = location
        self.geoLocation = geoLocation
        self.eucogSite = 'http://www.eucognition.org/eucog-wiki/' if event else 'http://www.eucognition.org/eucog-wiki/User:' + ID
        self.website = website
        self.imageUrl = imageUrl

pickledParticipantsFile = sys.argv[1]

outFileName = sys.argv[2]
csv_file = open(outFileName, 'w')

try:
    #Load the pickled participants
    pkl_file = open(pickledParticipantsFile, 'rb')
    participants = pickle.load(pkl_file)
    pkl_file.close()
except Exception as e:
    print e
    print 'Error reading pickled participants file'
    sys.exit()

#Now iterate over all participans and create the html file for leaflet map

for participant in participants:

    csv_file.write( participant.ID + \
                    ' , ' + participant.name + \
                    ' , ' + participant.location + \
                    ' , ' + str(participant.geoLocation[0]) + \
                    ' , ' + str(participant.geoLocation[1]) + \
                    ' , ' + participant.eucogSite + \
                    ' , ' + participant.website + \
                    '\n')
    
csv_file.close()

