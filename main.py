#Uncomment below if basemaps will be used
#from mpl_toolkits.basemap import Basemap
#import matplotlib.pyplot as plt

import numpy as np

import time
from googlegeocoder import GoogleGeocoder
import sys
import random
import cgi
import pickle

import leafletMapTemplate


class Participant:
    
    def __init__(self, ID, name, geoLocation, location, website, imageUrl='', event=False):
    
        self.ID = ID
        self.name = name
        self.location = location
        self.geoLocation = geoLocation
        self.eucogSite = 'http://www.eucognition.org/eucog-wiki/' if event else 'http://www.eucognition.org/eucog-wiki/User:' + ID
        self.website = website
        self.imageUrl = imageUrl

####  Initializations #########
eventLocationLon = -0.13131
eventLocationLat = 50.84294
eventLocation = 'Falmer/Brighton, UK'
eventWebsite = 'http://www.eucognition.org/index.php?page=2013-fourth-eucogiii-members-conference-gen-info'
eventName = 'Social and Ethical Aspects of Cognitive Systems'
eventID = ''

'''
participantsFile = 'BrightonParticipants.dat' # 'eucogMembers.dat' or 'BrightonParticipants.dat'
ignoreEventMarker = False # True for users without arcs or False for Events
ignoreArcs = False # True to draw arcs or False to ignore them
'''
participantsFile = 'eucogMembers.dat' # 'eucogMembers.dat' or 'BrightonParticipants.dat'
ignoreEventMarker = True # True for users without arcs or False for Events
ignoreArcs = True # True to draw arcs or False to ignore them
#####################


if(len(sys.argv) > 1):
    pickledParticipantsFile = sys.argv[1]
else:
    pickledParticipantsFile = ''



#popupTemplate = '''L.circle([%f, %f], 80, {color: 'red',fillColor: '#f03',fillOpacity: 0.5}).addTo(map).bindPopup(\'<b>%s</b><br /> <b>%s</b><br /> <a target="_blank" href=\"%s\">EUCog Wiki profile</a> | <a target="_blank" href=\"%s\">Web Page</a>\');\n\n'''

#popupTemplateNoWebsite = '''L.circle([%f, %f], 80, {color: 'red',fillColor: '#f03',fillOpacity: 0.5}).addTo(map).bindPopup(\'<b>%s</b><br /> <b>%s</b><br /> <a target="_blank" href=\"%s\">EUCog Wiki profile</a>\');\n\n'''


#If you prefer markers use the one below and comment out the one above
popupTemplate = '''markers.addLayer( new L.marker([%f, %f]).bindPopup(\'<b>%s</b><br /> <b>%s</b><br /> <a target="_blank" href=\"%s\">EUCog Wiki profile</a> | <a target="_blank" href=\"%s\">Web Page</a>\') );\n\n'''
popupTemplateNoWebsite = '''markers.addLayer( new L.marker([%f, %f]).bindPopup(\'<b>%s</b><br /> <b>%s</b><br /> <a target="_blank" href=\"%s\">EUCog Wiki profile</a>\')  );\n\n'''

arcTemplate = '''

    var start = new arc.Coord(%f, %f);
    var end = new arc.Coord(%f, %f);
    var gc = new arc.GreatCircle(start, end);
    var line = gc.Arc(20);

    L.geoJson(line.json()).addTo(map);

'''

leafletMapOutFilename = 'leafletMaps/'+participantsFile.split('.')[0] + '.html' 
leafletMapFile = open(leafletMapOutFilename, 'w')

fin = open(participantsFile, 'r')
allLines = fin.readlines()

js_participantsFilename = 'leafletMaps/js/'+participantsFile.split('.')[0] + '.js'
js_participantsFile = open(js_participantsFilename, 'w')


if(pickledParticipantsFile == ''):
    #Read all the data in the appropriate structures
    participants = []

    geocoder = GoogleGeocoder()

    for i in range(0, len(allLines), 2):
        
        print i
        
        elem = allLines[i].split(',')
        sites = allLines[i+1].split('|')
        
        if(len(sites) == 1): #No Website
            ID, name, website, location = elem[0].split(':')[1].strip(), elem[1].strip(), '', ' '.join(elem[-2:]).strip()
        else:
            ID, name, website, location = elem[0].split(':')[1].strip(), elem[1].strip(), sites[-1].strip(), ' '.join(elem[-2:]).strip()
        
        #Fetch geoLocation from Google
        
        time.sleep(0.5) #Google complains for very fast queries :)

        try:
            search = geocoder.get(location) 
            
            participantLat = search[0].geometry.location.lat + random.gauss(0, 0.002)  #Add some randomness for overlapping locations
            participantLon = search[0].geometry.location.lng + random.gauss(0, 0.002)
        
        except Exception as e: #Usually Zero results or too fast queries 
            time.sleep(0.5)
            print 'Error: ', e
            print '\tTrying only with the country ', location.split(' ')[-1]
            
            search = geocoder.get(location.split(' ')[-1]) 

            participantLat = search[0].geometry.location.lat + random.gauss(0, 0.002) 
            participantLon = search[0].geometry.location.lng + random.gauss(0, 0.002)


        participants.append( Participant(ID, name, (participantLat, participantLon), location, website) ) #This is used leaflet map 

    #Add the event as a participant
    if(not ignoreEventMarker):
        participants.append( Participant(eventID, eventName, (eventLocationLat, eventLocationLon), location, eventWebsite, event=True) ) #This is used leaflet map 

    #Save the vector of participants to avoid abusing Google Geocoder!
    #output = open('participants.pkl', 'wb')
    output = open('eucogMembers.pkl', 'wb')
    pickle.dump(participants, output)
    output.close()

else:
    
    try:
        #Load the pickled participants
        pkl_file = open(pickledParticipantsFile, 'rb')
        participants = pickle.load(pkl_file)
        pkl_file.close()
    except Exception as e:
        print e
        print 'Error reading pickled participants file'
        sys.exit()

#Now iterate over all participans and create the html file for leaflet map

markersCode = ''
greatArcCode = ''
for i in range(len(participants)):
    
    participant = participants[i]
    
    if(participant.website == ''):
        markersCode += popupTemplateNoWebsite % (participant.geoLocation[0], participant.geoLocation[1], participant.name.replace('\'', '&#39;'), participant.location.replace('\'', '&#39;'), participant.eucogSite)

    else:
        markersCode += popupTemplate % (participant.geoLocation[0], participant.geoLocation[1], participant.name.replace('\'', '&#39;'), participant.location.replace('\'', '&#39;'), participant.eucogSite, participant.website)

    if( not ignoreArcs and  i != len(participants)-1): #The last one is the event it self, we don't want to draw an arc
        greatArcCode += arcTemplate % (participant.geoLocation[1], participant.geoLocation[0], eventLocationLon, eventLocationLat)


#Write final html to file

leafletMapFile.write( leafletMapTemplate.HTML_TEMPLATE_FOR_LEAFLET % ('/'.join(js_participantsFilename.split('/')[1:]) ) )
leafletMapFile.close()

js_participantsFile.write(leafletMapTemplate.PARTICIPANTS_JS_TEMPLATE % (markersCode, greatArcCode))
js_participantsFile.close()

#The HTML Map part ends here
###############################################################################################



#Uncomment below if maptplotlib Basemaps will be used
'''
#Here we continue for matplotlib and Basemap


# create new figure, axes instances.
fig=plt.figure(figsize=(23.5, 13.0))
ax=fig.add_axes([0.1,0.1,0.8,0.8])

# setup mercator map projection.
m = Basemap(llcrnrlon=-120.,llcrnrlat=-40.,urcrnrlon=150.,urcrnrlat=70.,\
            rsphere=(6378137.00,6356752.3142),\
            resolution='h',projection='merc',\
            lat_0=60.,lon_0=-20.,lat_ts=20.)

for participant in participants:
        m.drawgreatcircle(eventLocationLon,eventLocationLat,participant.geoLocation[1],participant.geoLocation[0],linewidth=1,color='b')
    
m.drawcoastlines()
m.fillcontinents(color='#cc9966',lake_color='#99ffff')
m.drawmapboundary(fill_color='#99ffff')
m.drawcountries()
#m.drawparallels(np.arange(10,90,20),labels=[1,1,0,1])

# draw meridians
#m.drawmeridians(np.arange(-180,180,30),labels=[1,1,0,1])
ax.set_title('EUCog Network: ' + str(len(participants)-1) + ' Participants at Brighton Event')
plt.savefig(participantsFile.split('.')[0]+'.png', bbox_inches=0, dpi=200)

plt.show()
'''


