#Introduction

A python script that reads the list of participants of an EUCog event and draws a great circle route for each participant from their location to the event.

Please also read my blog post about it: []

#Dependencies

Numpy
matplotlib
Basemap from mpl_toolkits
GoogleGeocoder


#Please note: The Readme file is a not updated and the code is still changing. Go through main.py file if something is not working as expected or is not aligned from the steps below.

#How to use

###Step 1:
Visit the participant's list at EUCog website, e.g., for the Fourth EUCogIII Members Conference at Falmer/Brighton go at [http://www.eucognition.org/index.php?page=2013-fourth-eucogiii-members-conference-list-of-participants] 

###Step 2:
Copy the complete list and save it in a text file, e.g., see the ```BrightonParticipants.dat''' file. WARNING: Make sure you don't leave an extra empty new line at the end.

Note: The content in the website is dynamic therefore parsing the html from python without a headless browser (a web browser without a graphical user interface). However, these events happen twice a year so I dindn't want to have an additional dependency just for that :) It only takes a second. For different applications you may want to consider fetching the web page with a headless browser.


###Step 3:
In the main.py file, add the required information, e.g., 

```
####  Initializations #########
eventLocationLon = -0.13131
eventLocationLat = 50.84294
eventLocation = 'Falmer/Brighton, UK'
eventWebsite = 'http://www.eucognition.org/index.php?page=2013-fourth-eucogiii-members-conference-gen-info'
eventName = 'Social and Ethical Aspects of Cognitive Systems'
eventID = '' #That should be empty for the events

participantsFile = 'BrightonParticipants.dat' #This is the file with list of participants from step 1
#####################
```


###Step 4:

If you have the participants.pkl file in your folder then it means that the geolocations are already there so you can run

```
python main.py participants.pkl
```

Otherwise you can run 

```
python main.py
```
and it will query google with all the locations.

You can either interact with the Basemap map (Zoom in/Zoom out), or open the saved image sameWithParticipantsFileName.png in the same folder, or open the html file for the interactive map in leafletMaps/sameWithParticipansFileName.html




